package org.example;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class Menu {
    private static final EntityManagerFactory emf = Persistence.createEntityManagerFactory("db_banque");
    static EntityManager em = emf.createEntityManager();
    public static void affmenu() {
        Scanner sc = new Scanner(System.in);
        System.out.println("----Menu Banque----");
        System.out.println("1. Créer un client");
        System.out.println("2. Créer un compte");
        System.out.println("3. Créer une agence");
        System.out.println("4. Exit");
    }

    public static void createClient() throws ParseException {
        em.getTransaction().begin();
        Scanner sc = new Scanner(System.in);
        System.out.println("Vous avez choisi de créer un client.");
        System.out.println("Veuillez saisir le prénom du client: ");
        String prenom = sc.next();
        prenom += sc.nextLine();
        System.out.println();
        System.out.println("Veuillez saisir le nom de famille du client: ");
        String nom = sc.next();
        nom += sc.nextLine();
        System.out.println();
        System.out.println("Veuillez saisir la date de naissance du client: ");
        String date = sc.next();
        date += sc.nextLine();
        SimpleDateFormat f = new SimpleDateFormat("dd-MM-yyyy");
        Date dateNaissance = f.parse(date);
        Client client = new Client(prenom, nom, dateNaissance);
        if (client == null) {
            System.out.println("Erreur dans la création du client, veuillez réessayer.");
            createClient();
        } else {
            System.out.println("Le client a bien été créé!");
            em.persist(client);
            em.getTransaction().commit();
        }
    }

    public static void createCompte(){
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        Scanner sc = new Scanner(System.in);
        System.out.println("Vous avez choisi de créer un compte.");
        System.out.println("Veuillez saisir un libelle: ");
        String libelle = sc.next();
        libelle += sc.nextLine();
        System.out.println();
        System.out.println("Veuillez saisir un IBAN: ");
        String iban = sc.next();
        iban += sc.nextLine();
        System.out.println();
        System.out.println("Veuillez saisir un solde: ");
        double solde = sc.nextDouble();
        System.out.println();
        Compte compte = new Compte(libelle, iban, solde);
        if (compte == null){
            System.out.println("Erreur dans la création du client, veuillez réessayer.");
            createCompte();
        } else{
            System.out.println("Le compte a bien été créé!");
            em.persist(compte);
            em.getTransaction().commit();
        }
    }

    public static void createAgence(){
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        Scanner sc = new Scanner(System.in);
        System.out.println("Vous avez choisi de créer une agence.");
        System.out.println("Veuillez saisir son adresse: ");
        String address = sc.next();
        address += sc.nextLine();
        Agence agence = new Agence(address);
        if (agence == null){
            System.out.println("Erreur dans la création de l'agence. Veuillez réessayer.");
            createAgence();
        } else {
            System.out.println("L'agence a bien été créé!");
            em.persist(agence);
            em.getTransaction().commit();
        }
    }


    public static void menu() throws ParseException {
        int choix = 0;
        do{
            Scanner sc = new Scanner(System.in);
            affmenu();
            choix = sc.nextInt();
            switch (choix){
                case 1: createClient();
                break;
                case 2: createCompte();
                break;
                case 3: createAgence();
                break;
                default: break;
            }
        } while(choix != 4);
        if (choix==4){
            em.close();
            emf.close();
        }

    }


}
