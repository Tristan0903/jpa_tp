package org.example;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "compte")
public class Compte {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String libelle;

    private String iban;

    private double solde;

    @ManyToOne
    @JoinColumn(name = "id_agence")
    private Agence agence;

    @ManyToMany(mappedBy = "compteList")
    private List<Client> clientList = new ArrayList<>();


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public String getIban() {
        return iban;
    }

    public void setIban(String iban) {
        this.iban = iban;
    }

    public double getSolde() {
        return solde;
    }

    public void setSolde(double solde) {
        this.solde = solde;
    }

    public Agence getAgence() {
        return agence;
    }

    public void setAgence(Agence agence) {
        this.agence = agence;
    }

    public List<Client> getClientList() {
        return clientList;
    }

    public void setClientList(List<Client> clientList) {
        this.clientList = clientList;
    }

    public Compte() {
    }

    public Compte(String libelle, String iban, double solde, Agence agence) {
        this.libelle = libelle;
        this.iban = iban;
        this.solde = solde;
        this.agence = agence;
    }

    public Compte(String libelle, String iban, double solde) {
        this.libelle = libelle;
        this.iban = iban;
        this.solde = solde;
    }

    @Override
    public String toString() {
        return "Compte{" +
                "id=" + id +
                ", libelle='" + libelle + '\'' +
                ", iban='" + iban + '\'' +
                ", solde=" + solde +
                ", agence=" + agence +
                ", clientList=" + clientList +
                '}';
    }
}
