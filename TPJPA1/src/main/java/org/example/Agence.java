package org.example;

import javax.persistence.*;

@Entity
@Table(name = "agence")
public class Agence {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long agence_Id;

    private String address;

    public Agence() {
    }

    public Long getAgenceId() {
        return agence_Id;
    }

    public void setAgenceId(Long agenceId) {
        this.agence_Id = agenceId;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Agence(String address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "Agence{" +
                "agenceId=" + agence_Id +
                ", address='" + address + '\'' +
                '}';
    }
}
