package org.example.demo;

import org.example.entity.manyToMany.Post;
import org.example.entity.manyToMany.Tag;

import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class Demo2 {
    private static final EntityManagerFactory emf = Persistence.createEntityManagerFactory("jpa_demo");

    public static void main(){
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();

        Post post = new Post("La formation c'est trop bien");
        Post post2 = new Post("Je déteste le formateur");

        Tag tag1 = new Tag();
        tag1.setName("Java");
        Tag tag2 = new Tag();
        tag2.setName("Javascript");

        post.addTag(tag1);
        post.addTag(tag2);
        post2.addTag(tag2);

        em.persist(post);
        em.persist(post2);

        em.getTransaction().commit();
        em.close();
        emf.close();
    }
}
